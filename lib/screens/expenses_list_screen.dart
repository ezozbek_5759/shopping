import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:shopping_new/model/expense.dart';

class ExpensesList extends StatelessWidget {
  final double screenHeight;
  final List<Expense> expenseList;

  const ExpensesList({
    Key? key,
    required this.screenHeight,
    required this.expenseList,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Positioned(
      bottom: 0,
      child: Container(
        height: screenHeight * 0.61,
        width: MediaQuery.of(context).size.width,
        child: Card(
          margin: EdgeInsets.all(0),
          color: Colors.white,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(32),
              topRight: Radius.circular(32),
            ),
          ),
          child: ListView.builder(
            itemBuilder: (context, index) {
              return ListTile(
                leading: CircleAvatar(
                  backgroundColor: expenseList[index].color,
                ),
                title: Text(
                    "${expenseList[index].title} - ${expenseList[index].amount.toStringAsFixed(0)} so'm"),
                subtitle: Text(
                  DateFormat.yMMMMd().format(expenseList[index].date),
                ),
                trailing: Icon(
                  Icons.delete_outline,
                ),
              );
            },
            itemCount: expenseList.length,
          ),
        ),
      ),
    );
  }
}
