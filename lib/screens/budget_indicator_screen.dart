import 'package:flutter/material.dart';

class BudgetIndicatorScreen extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Card(
                  margin: EdgeInsets.all(0),
                  color: Colors.grey.shade200,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(32),
                      topRight: Radius.circular(32),
                    ),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 20,
                      vertical: 10,
                    ),
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(
                              children: [
                                Text("Oylik byudjet:"),
                                TextButton(
                                  onPressed: () {},
                                  child: Text(
                                    "800,000 so'm",
                                  ),
                                ),
                              ],
                            ),
                            Text(
                              "0%",
                            ),
                          ],
                        ),
                        Container(
                          alignment: Alignment.centerLeft,
                          width: double.infinity,
                          height: 10,
                          decoration: BoxDecoration(
                            color: Colors.grey,
                            borderRadius: BorderRadius.circular(13),
                          ),
                          child: FractionallySizedBox(
                            widthFactor: 0.7,
                            child: Container(
                              decoration: BoxDecoration(
                                  // color: Colors.red,
                                  gradient: LinearGradient(
                                    colors: [
                                      Colors.blue.shade200,
                                      Colors.blue.shade300,
                                      Colors.blue.shade400,
                                      Colors.blue.shade500,
                                      Colors.blue.shade600,
                                      Colors.blue.shade700,
                                    ],
                                  ),
                                  borderRadius: BorderRadius.circular(13),
                                  boxShadow: [
                                    BoxShadow(
                                      color: Colors.blue.withOpacity(.5),
                                      blurRadius: 10.0,
                                      spreadRadius: 1,
                                      offset: Offset.fromDirection(7),
                                    ),
                                  ]),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                );
  }
}