import 'package:flutter/material.dart';

class NewExpenseScreen extends StatefulWidget {
  final Function addNewExpense;
  final Function openDateTimePicker;
  NewExpenseScreen(this.addNewExpense, this.openDateTimePicker);

  @override
  _NewExpenseScreenState createState() => _NewExpenseScreenState();
}

class _NewExpenseScreenState extends State<NewExpenseScreen> {
  final titleController = TextEditingController();

  final amountController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(20),
      child: Column(
        children: [
          TextField(
            decoration: InputDecoration(labelText: "Expense name"),
            keyboardType: TextInputType.text,
            controller: titleController,
          ),
          TextField(
            decoration: InputDecoration(labelText: "Expense amount"),
            keyboardType: TextInputType.number,
            controller: amountController,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                "Xarajat kunini tanlash",
              ),
              TextButton(
                onPressed: () {
                  widget.openDateTimePicker();
                },
                child: Text("Xarajat kuni tanlanmagan"),
              ),
            ],
          ),
          ElevatedButton.icon(
            onPressed: () => widget.addNewExpense(
              titleController.text,
              double.parse(amountController.text),
            ),
            icon: Icon(Icons.add),
            label: Text("Add"),
          ),
        ],
      ),
    );
  }
}
