import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class CalendarScreen extends StatelessWidget {
  final double screenHeight;
  final List months;
  final DateTime selectedDate;
  final Function presentMonthPicker;
  final Function() previousMonth;
  final Function() nextMonth;

  const CalendarScreen({
    Key? key,
    required this.screenHeight,
    required this.months,
    required this.selectedDate,
    required this.presentMonthPicker,
    required this.previousMonth,
    required this.nextMonth,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: screenHeight * 0.17,
      child: Column(
        children: [
          TextButton(
            onPressed: () => presentMonthPicker(context),
            child: Text(
              '${months[selectedDate.month - 1]} ${DateFormat.y().format(selectedDate)}',
              style: TextStyle(
                color: Colors.grey,
                fontSize: 20,
                fontFamily: 'BreeSerif',
              ),
            ),
          ),
          SizedBox(
            height: 12,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Container(
                height: 40,
                width: 40,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(40),
                  border: Border.all(
                    color:
                        (selectedDate.year == 2021 && selectedDate.month == 1)
                            ? Colors.grey
                            : Colors.black,
                  ),
                ),
                child: IconButton(
                  onPressed: () => previousMonth(),
                  icon: Icon(
                    Icons.arrow_left,
                    color:
                        (selectedDate.year == 2021 && selectedDate.month == 1)
                            ? Colors.grey
                            : Colors.black,
                  ),
                ),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Text(
                    "15,000",
                    style: TextStyle(
                      fontSize: 43,
                      fontFamily: 'BreeSerif',
                    ),
                  ),
                  Text(
                    "so'm",
                    style: TextStyle(
                      fontSize: 20,
                      height: 0.5,
                      fontFamily: 'BreeSerif',
                    ),
                  ),
                ],
              ),
              Container(
                height: 40,
                width: 40,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(40),
                  border: Border.all(
                    color: (selectedDate.year == 2021 &&
                            selectedDate.month == DateTime.now().month)
                        ? Colors.grey
                        : Colors.black,
                  ),
                ),
                child: IconButton(
                  onPressed: () => nextMonth(),
                  icon: Icon(
                    Icons.arrow_right,
                    color: (selectedDate.year == 2021 &&
                            selectedDate.month == DateTime.now().month)
                        ? Colors.grey
                        : Colors.black,
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
