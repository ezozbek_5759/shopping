import 'package:flutter/material.dart';

import 'package:shopping_new/screens/calendar_screen.dart';
import 'package:shopping_new/screens/budget_indicator_screen.dart';

// import '../model/expense.dart';
import '../data/expenses.dart';
import '../screens/expenses_list_screen.dart';
import '../screens/new_expense_screen.dart';
import 'model/expense.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  static const List _months = [
    'Yanvar',
    'Fevral',
    'Mart',
    'Aprel',
    'May',
    'Iyun',
    'Iyul',
    'Avgust',
    'Sentabr',
    'Oktabr',
    'Noyabr',
    'Dekabr',
  ];

  DateTime _selectedDate = DateTime.now();
  late DateTime _newExpenseDate;

  void _presentMonthPicker(BuildContext context) {
    // print("Oyni korsat!");
    showMonthPicker(
      context: context,
      initialDate: _selectedDate,
      firstDate: DateTime(2021),
      lastDate: DateTime.now(),
    ).then((value) => {
          if (value != null)
            {
              setState(() {
                _selectedDate = value;
              }),
            }
        });
  }

  void _previousMonth() {
    // print("Oldingi oy");
    final previousDate = DateTime(_selectedDate.year, _selectedDate.month - 1);
    if (previousDate.year > 2020) {
      setState(() {
        _selectedDate = previousDate;
      });
    }
  }

  void _nextMonth() {
    // print("Keyingi oy");
    final nextDate = DateTime(_selectedDate.year, _selectedDate.month + 1);
    if (nextDate.month <= DateTime.now().month) {
      setState(() {
        _selectedDate = nextDate;
      });
    }
  }

  void presentWindowNewExpense(BuildContext context) {
    showModalBottomSheet(
        context: context,
        builder: (_) {
          return NewExpenseScreen(addNewExpense, openDateTimePicker);
        });
  }

  void openDateTimePicker() {
    showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(2021),
      lastDate: DateTime.now(),
    ).then((value) {
      if(value == null){
        return;
      }
      setState(() {
        _newExpenseDate = value;
      });
    });
  }

  void addNewExpense(String title, double amount) {
    final newExpense = Expense(
      id: "${expenses.length + 1}",
      title: title,
      amount: amount,
      date: _newExpenseDate,
    );
    setState(() {
      expenses.add(newExpense);
    });
    Navigator.of(context).pop();
  }

  @override
  Widget build(BuildContext context) {
    final appBar = AppBar(
      title: Text(
        "Xarajatlar",
        style: TextStyle(
          fontFamily: 'BreeSerif',
        ),
      ),
      actions: [
        IconButton(
          onPressed: () => presentWindowNewExpense(context),
          icon: Icon(Icons.add),
        ),
      ],
    );

    final screenHeight =
        MediaQuery.of(context).size.height - appBar.preferredSize.height;
    return Scaffold(
      appBar: appBar,
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          CalendarScreen(
            screenHeight: screenHeight,
            months: _months,
            nextMonth: _nextMonth,
            previousMonth: _previousMonth,
            presentMonthPicker: _presentMonthPicker,
            selectedDate: _selectedDate,
          ),
          Positioned(
            bottom: 0,
            child: Container(
              height: screenHeight * 0.73,
              child: Stack(
                children: [
                  BudgetIndicatorScreen(),
                  ExpensesList(
                    screenHeight: screenHeight,
                    expenseList: expenses,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  showMonthPicker({required BuildContext context, DateTime initialDate, DateTime firstDate, DateTime lastDate}) {}
}
