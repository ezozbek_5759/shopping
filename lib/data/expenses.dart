import 'package:flutter/material.dart';
import 'package:shopping_new/model/expense.dart';

List<Expense> expenses = [
  Expense(id: "e1", title: "Tarvuz", amount: 22000, color: Colors.green, date: DateTime.now()),
  Expense(id: "e2", title: "Krassovka", amount: 100000, color: Colors.red, date: DateTime.now()),
  Expense(id: "e3", title: "Paynet", amount: 89000, color: Colors.blue, date: DateTime.now()),
  Expense(id: "e4", title: "Taxi", amount: 15000, color: Colors.yellow, date: DateTime.now()),
];
Odamlar kompyuterni ma'qul ko'rishadi. Kompyuterda yozilgan ma'lumotlar qo'lda yozilgan ma'lumotlarga qaraganda havfsizroq saqlanadi. faqatgina kundalik hayotda ba'zi narsalarni qo'lda yozib borishlari mumkin
People like computers. Data recorded on a computer is stored more securely than data recorded manually. they can only write down some things by hand in daily life